Projeto de Nocoes de Sistemas Operacionais
==========================================

Esse projeto tem como objetivo implementar um pseudo-SO multiprogramado, composto por um Gerenciador de Processos, por um Gerenciador de Memoria, e por um Gerenciador de Recursos.

Cada gerenciador deve desempenhar suas funcoes basicas, no caso:
 - Gerenciador de Processos: Agrupar processos em quatro niveis de prioridades;
 - Gerenciador de Memoria: Garantir que um processo nao acesse regioes de memoria de outro processo;
 - Gerenciador de Recursos: Administrar a alocacao e liberacao de recursos disponiveis
 
TODO: Colocar aqui os detalhes de implementacao
 
Estruturamento do Programa:
 
 - Modulo de Processos : Classes e estruturas de dados relativas ao processo. Basicamente mantem informacoes especificas do processo
 - Modulo de Filas : Mantem as interfaces e funcoes que operam sobre os processos
 - Modulo de Memoria : prove uma interface de abstracao da RAM
 - Modulo de Recurso - trata a alocacao e liberacoa dos recursos de E/S para os processos
 