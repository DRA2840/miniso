package br.unb.nso.pseudoSO.dispositivo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DispositivoMementoTest {
	
	private DispositivoMemento teste;
	private Dispositivo aux;
	
	@Before
	public void setUp(){
		teste = new DispositivoMemento();
		aux = new Dispositivo();
	}
	
	@Test
	public void testGetSetImpressora1(){
		teste.setImpressora1(aux);
		assertEquals(aux, teste.getImpressora1());
	}
	
	@Test
	public void testGetSetImpressora2(){
		teste.setImpressora2(aux);
		assertEquals(aux, teste.getImpressora2());
	}
	
	@Test
	public void testGetSetScanner(){
		teste.setScanner(aux);
		assertEquals(aux, teste.getScanner());
	}
	
	@Test
	public void testGetSetModem(){
		teste.setModem(aux);
		assertEquals(aux, teste.getModem());
	}
	
	@Test
	public void testGetSetDisco1(){
		teste.setDisco1(aux);
		assertEquals(aux, teste.getDisco1());
	}
	
	@Test
	public void testGetSetDisco2(){
		teste.setDisco2(aux);
		assertEquals(aux, teste.getDisco2());
	}
	
}
