package br.unb.nso.pseudoSO.dispositivo;

import org.junit.Before;
import org.junit.Test;

import br.unb.nso.pseudoSO.kernel.Kernel;
import br.unb.nso.pseudoSO.processo.Processo;
import br.unb.nso.pseudoSO.processo.ProcessoBloqueadoException;
import br.unb.nso.pseudoSO.processo.RequisicaoProcesso;

public class GerenciadorDispositivosTest {
	
	private GerenciadorDispositivos teste;
	
	@Before
	public void setUp(){
		this.teste = new GerenciadorDispositivos();
	}
	
	@Test
	public void alocaImpressora1Okay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 1, 0, 0, 0"), 0);
		
		teste.requisitarDipositivos(aux);
	}
	
	@Test(expected=ProcessoBloqueadoException.class)
	public void alocaImpressora1DepoisDeOutroProcessoNotOkay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 1, 0, 0, 0"), 0);
		teste.requisitarDipositivos(aux);
		
		//Outro processo requisitando a mesma impressora
		Processo aux2 = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 1, 0, 0, 0"), 0);
		teste.requisitarDipositivos(aux2);
	}
	
	@Test
	public void alocaImpressora1DepoisDeLiberarOkay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 1, 0, 0, 0"), 0);
		teste.requisitarDipositivos(aux);
		
		teste.liberarDispositivos(new Kernel(), aux);
		
		//Outro processo requisitando a mesma impressora
		Processo aux2 = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 1, 0, 0, 0"), 0);
		teste.requisitarDipositivos(aux2);
	}
	
	@Test
	public void alocaImpressora2Okay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 2, 0, 0, 0"), 0);
		
		teste.requisitarDipositivos(aux);
	}
	
	@Test(expected=ProcessoBloqueadoException.class)
	public void alocaImpressora2DepoisDeOutroProcessoNotOkay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 2, 0, 0, 0"), 0);
		teste.requisitarDipositivos(aux);
		
		//Outro processo requisitando a mesma impressora
		Processo aux2 = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 2, 0, 0, 0"), 0);
		teste.requisitarDipositivos(aux2);
	}
	
	@Test
	public void alocaImpressora2DepoisDeLiberarOkay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 2, 0, 0, 0"), 0);
		teste.requisitarDipositivos(aux);
		
		teste.liberarDispositivos(new Kernel(), aux);
		
		//Outro processo requisitando a mesma impressora
		Processo aux2 = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 2, 0, 0, 0"), 0);
		teste.requisitarDipositivos(aux2);
	}
	
	@Test
	public void alocaScannerOkay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 1, 0, 0"), 0);
		
		teste.requisitarDipositivos(aux);
	}
	
	@Test(expected=ProcessoBloqueadoException.class)
	public void alocaScannerDepoisDeOutroProcessoNotOkay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 1, 0, 0"), 0);
		teste.requisitarDipositivos(aux);
		
		//Outro processo requisitando a mesma impressora
		Processo aux2 = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 1, 0, 0"), 0);
		teste.requisitarDipositivos(aux2);
	}
	
	@Test
	public void alocaScannerDepoisDeLiberarOkay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 1, 0, 0"), 0);
		teste.requisitarDipositivos(aux);
		
		teste.liberarDispositivos(new Kernel(), aux);
		
		//Outro processo requisitando a mesma impressora
		Processo aux2 = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 1, 0, 0"), 0);
		teste.requisitarDipositivos(aux2);
	}
	
	@Test
	public void alocaModemOkay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 1, 0"), 0);
		
		teste.requisitarDipositivos(aux);
	}
	
	@Test(expected=ProcessoBloqueadoException.class)
	public void alocaModemDepoisDeOutroProcessoNotOkay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 1, 0"), 0);
		teste.requisitarDipositivos(aux);
		
		//Outro processo requisitando a mesma impressora
		Processo aux2 = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 1, 0"), 0);
		teste.requisitarDipositivos(aux2);
	}
	
	@Test
	public void alocaModemDepoisDeLiberarOkay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 1, 0"), 0);
		teste.requisitarDipositivos(aux);
		
		teste.liberarDispositivos(new Kernel(), aux);
		
		//Outro processo requisitando a mesma impressora
		Processo aux2 = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 1, 0"), 0);
		teste.requisitarDipositivos(aux2);
	}
	
	@Test
	public void alocaDisco1Okay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 1"), 0);
		
		teste.requisitarDipositivos(aux);
	}
	
	@Test(expected=ProcessoBloqueadoException.class)
	public void alocaDisco1DepoisDeOutroProcessoNotOkay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 1"), 0);
		teste.requisitarDipositivos(aux);
		
		//Outro processo requisitando a mesma impressora
		Processo aux2 = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 1"), 0);
		teste.requisitarDipositivos(aux2);
	}
	
	@Test
	public void alocaDisco1DepoisDeLiberarOkay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 1"), 0);
		teste.requisitarDipositivos(aux);
		
		teste.liberarDispositivos(new Kernel(), aux);
		
		//Outro processo requisitando a mesma impressora
		Processo aux2 = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 1"), 0);
		teste.requisitarDipositivos(aux2);
	}
	
	@Test
	public void alocaDisco2Okay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 2"), 0);
		
		teste.requisitarDipositivos(aux);
	}
	
	@Test(expected=ProcessoBloqueadoException.class)
	public void alocaDisco2DepoisDeOutroProcessoNotOkay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 2"), 0);
		teste.requisitarDipositivos(aux);
		
		//Outro processo requisitando a mesma impressora
		Processo aux2 = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 2"), 0);
		teste.requisitarDipositivos(aux2);
	}
	
	@Test
	public void alocaDisco2DepoisDeLiberarOkay() throws ProcessoBloqueadoException{
		//processo que requisita impressora 1
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 2"), 0);
		teste.requisitarDipositivos(aux);
		
		teste.liberarDispositivos(new Kernel(), aux);
		
		//Outro processo requisitando a mesma impressora
		Processo aux2 = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 2"), 0);
		teste.requisitarDipositivos(aux2);
	}
	
}
