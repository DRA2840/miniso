package br.unb.nso.pseudoSO.dispositivo;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class DispositivoTest {

	private Dispositivo teste;

	@Before
	public void setUp() {
		this.teste = new Dispositivo();
	}

	@Test
	public void downEmDispositivoIniciadoOkay() {
		assertTrue(this.teste.down());
	}

	@Test
	public void downsConsecutivosNotOkay() {
		this.teste.down();
		assertFalse(this.teste.down());
		assertFalse(this.teste.down());
		assertFalse(this.teste.down());
	}

	@Test
	public void downAfterUpIsOkay() {
		this.teste.down();
		this.teste.up();

		assertTrue(this.teste.down());
	}
	
	@Test
	public void cloneTest(){
		Dispositivo aux = this.teste.clone();
		assertTrue(this.teste.down());
		assertTrue(aux.down());
	}
}
