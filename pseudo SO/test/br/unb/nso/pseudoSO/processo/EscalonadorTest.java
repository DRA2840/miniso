package br.unb.nso.pseudoSO.processo;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EscalonadorTest {

	private Escalonador teste;
	
	@Before
	public void setUp(){
		this.teste = new Prioridade();
	}
	
	@Test
	public void testNumProcessosComAddAndRemove(){
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 0"), 0);
		
		assertEquals(0, this.teste.getNumTotalProcessos());
		assertEquals(0, this.teste.getNumProcessosProntos());
		
		teste.adicionarProcesso(aux);
		
		assertEquals(1, this.teste.getNumTotalProcessos());
		assertEquals(1, this.teste.getNumProcessosProntos());
		
		try {
			teste.bloquearProcesso(aux);
			fail();
		} catch (ProcessoBloqueadoException e) {
			assertEquals(1, this.teste.getNumTotalProcessos());
			assertEquals(0, this.teste.getNumProcessosProntos());
		}
		
		List<Processo> aux2 = new LinkedList<Processo>();
		aux2.add(aux);
		
		teste.desbloquearProcessos(aux2);
		
		assertEquals(1, this.teste.getNumTotalProcessos());
		assertEquals(1, this.teste.getNumProcessosProntos());
		
		teste.encerrarProcesso(aux);
		
		assertEquals(0, this.teste.getNumTotalProcessos());
		assertEquals(0, this.teste.getNumProcessosProntos());
		
	}
	
	@Test
	public void testEnvelhecerProcessos() throws SecurityException, NoSuchFieldException, NumberFormatException, IllegalArgumentException, IllegalAccessException{
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 0"), 0);
		
		teste.adicionarProcesso(aux);		
		teste.envelhecerProcessos();
		
		Field aux2 = Processo.class.getDeclaredField("idade");
		aux2.setAccessible(true);
		
		int idade = (Integer) aux2.get(aux);
		
		assertEquals(1, idade);	
	}
	
	@Test
	public void testEscalonar(){
		Processo aux = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 0"), 0);
		
		teste.adicionarProcesso(aux);
		
		assertEquals(aux, teste.escalonar());
	}
}
