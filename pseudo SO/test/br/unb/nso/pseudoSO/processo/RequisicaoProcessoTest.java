package br.unb.nso.pseudoSO.processo;

import static org.junit.Assert.*;

import org.junit.Test;

public class RequisicaoProcessoTest {

	@Test
	public void compararRequisicoes(){
		
		RequisicaoProcesso req1 = new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 0");
		RequisicaoProcesso req2 = new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 0");
		RequisicaoProcesso req3 = new RequisicaoProcesso("1, 0, 0, 0, 0, 0, 0, 0");
		RequisicaoProcesso req4 = new RequisicaoProcesso("2, 0, 0, 0, 0, 0, 0, 0");
		
		assertEquals("Requisicoes no mesmo tempo", req1.compareTo(req2), 0);
		assertTrue("Requisicoes em tempos diferentes", req1.compareTo(req3) < 0);
		assertTrue("Requisicoes em tempos diferentes", req1.compareTo(req4) < 0);
		assertTrue("Requisicoes em tempos diferentes", req3.compareTo(req1) > 0);
		assertTrue("Requisicoes em tempos diferentes", req4.compareTo(req1) > 0);
	}
	
	@Test
	public void testToString(){
		String teste = "1, 2, 3, 4, 5, 6, 7, 8";
		RequisicaoProcesso req1 = new RequisicaoProcesso(teste);
		
		assertEquals(teste, req1.toString());
	}
	
}
