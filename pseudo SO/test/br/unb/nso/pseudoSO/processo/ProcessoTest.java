package br.unb.nso.pseudoSO.processo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;

import org.junit.Test;

import br.unb.nso.pseudoSO.kernel.Kernel;

public class ProcessoTest {

	@Test
	public void testCompare(){

		RequisicaoProcesso req1 = new RequisicaoProcesso("0, 3, 0, 0, 0, 0, 0, 0");
		RequisicaoProcesso req2 = new RequisicaoProcesso("1, 2, 0, 0, 0, 0, 0, 0");
		RequisicaoProcesso req3 = new RequisicaoProcesso("2, 1, 0, 0, 0, 0, 0, 0");
		RequisicaoProcesso req4 = new RequisicaoProcesso("3, 0, 0, 0, 0, 0, 0, 0");
		RequisicaoProcesso req5 = new RequisicaoProcesso("4, 0, 0, 0, 0, 0, 0, 0");

		Processo processo1 = new Processo(req1, 0);
		Processo processo2 = new Processo(req2, 0);
		Processo processo3 = new Processo(req3, 0);
		Processo processo4 = new Processo(req4, 0);
		Processo processo5 = new Processo(req5, 0);

		assertEquals( processo5.compareTo(processo5), 0);		
		assertTrue( processo5.compareTo(processo3) < 0);
		assertTrue( processo5.compareTo(processo2) < 0);
		assertTrue( processo5.compareTo(processo1) < 0);
		assertEquals( processo4.compareTo(processo4), 0);
		assertTrue( processo4.compareTo(processo5) < 0);
		assertTrue( processo4.compareTo(processo3) < 0);
		assertTrue( processo4.compareTo(processo2) < 0);
		assertTrue( processo4.compareTo(processo1) < 0);
		assertEquals( processo3.compareTo(processo3), 0);
		assertTrue( processo3.compareTo(processo2) < 0);
		assertTrue( processo3.compareTo(processo1) < 0);
		assertEquals( processo2.compareTo(processo2), 0);
		assertTrue( processo2.compareTo(processo1) < 0);

	}

	@Test
	public void testToString() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{

		Processo p1 = new Processo(new RequisicaoProcesso("0, 0, 0, 0, 0, 0, 0, 0"), 0);

		Field aux2 = Processo.class.getDeclaredField("numProcessos");
		aux2.setAccessible(true);

		int numProcessos = (Integer) aux2.get(p1);

		String aux = "\tPID: " + numProcessos + "\n\toffset: 0\n\tblocks: 0\n\tpriority: 0\n\ttime:"
				+ " 0\n\tprinters: 0\n\tscanners: 0\n\tmodem: 0\n\tdrives: 0";

		assertEquals( aux, p1.toString());
	}

	@Test
	public void testProcessarKernel() throws ProcessoBloqueadoException, SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
		Processo p1 = new Processo(new RequisicaoProcesso("0, 0, 1, 0, 0, 0, 0, 0"), 0);
		
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
			System.setOut(new PrintStream(outContent));
		
		p1.processar(new Kernel());
		
		Field aux2 = Processo.class.getDeclaredField("numProcessos");
		aux2.setAccessible(true);

		int numProcessos = (Integer) aux2.get(p1);

		ByteArrayOutputStream aux = new ByteArrayOutputStream();
		PrintStream printer = new PrintStream(aux);
		printer.println("\tP" + numProcessos + "  instruction 1");
		
		assertEquals(aux.toString(), outContent.toString());

	}


}
