package br.unb.nso.pseudoSO.memoria;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class GerenciadorMemoriaTest {

	private GerenciadorMemoria teste;

	@Before
	public void setUp() {
		this.teste = new GerenciadorMemoria();
	}

	@Test
	public void verificarMemoriaOkay() throws MemoriaInsuficienteException {
		assertTrue(teste.existemBlocosContiguos(64, 0));
		assertTrue(teste.existemBlocosContiguos(960, 1));
	}

	@Test(expected = MemoriaInsuficienteException.class)
	public void memoriaVerificadaMaiorQueAMaximaDeUsuario()
			throws MemoriaInsuficienteException {
		teste.existemBlocosContiguos(961, 1);
	}

	@Test(expected = MemoriaInsuficienteException.class)
	public void memoriaVerificadaMaiorQueAMaximaDeKernel()
			throws MemoriaInsuficienteException {
		teste.existemBlocosContiguos(65, 0);
	}

	@Test
	public void possivelAlocarMemoriaPAraVariosProcessos()
			throws MemoriaInsuficienteException {
		int offset1 = teste.alocarMemoria(32, 0);
		int offset2 = teste.alocarMemoria(32, 0);
		int offset3 = teste.alocarMemoria(60, 1);
		int offset4 = teste.alocarMemoria(400, 2);
		int offset5 = teste.alocarMemoria(300, 3);

		assertEquals(0, offset1);
		assertEquals(32, offset2);
		assertEquals(64, offset3);
		assertEquals(124, offset4);
		assertEquals(524, offset5);
	}

	@Test
	public void naoTemMemoriaDepoisDeAlocar()
			throws MemoriaInsuficienteException {
		teste.alocarMemoria(64, 0);
		teste.alocarMemoria(960, 1);
		assertFalse(teste.existemBlocosContiguos(64, 0));
		assertFalse(teste.existemBlocosContiguos(960, 1));
	}
	
	@Test
	public void liberarMemoriaOkay()
			throws MemoriaInsuficienteException {
		int offset1 = teste.alocarMemoria(64, 0);
		int offset2 = teste.alocarMemoria(960, 1);
		assertFalse(teste.existemBlocosContiguos(64, 0));
		assertFalse(teste.existemBlocosContiguos(960, 1));
		
		teste.liberarMemoria(64, offset1);
		teste.liberarMemoria(960, offset2);
		
		assertTrue(teste.existemBlocosContiguos(64, 0));
		assertTrue(teste.existemBlocosContiguos(960, 1));
	}

}
