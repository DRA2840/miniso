package br.unb.nso.pseudoSO.processo;

/**
 * Excecao que ocorre quando um processo tenta alocar um dispositivo de I/O
 * previamente alocado para outro processo
 * 
 * @author Diego Azevedo
 * 
 */
@SuppressWarnings("serial")
public class ProcessoBloqueadoException extends Exception {

}
