package br.unb.nso.pseudoSO.processo;

/**
 * Classe que representa a requisicao de um processo
 * 
 * @author Diego Azevedo
 * 
 */
public class RequisicaoProcesso implements Comparable<RequisicaoProcesso> {

	private static final int TEMPO_INICIALIZACAO = 0;
	private static final int PRIORIDADE = 1;
	private static final int TEMPO_PROCESSADOR = 2;
	private static final int BLOCOS_MEMORIA= 3;
	private static final int NUM_IMPRESSORA = 4;
	private static final int REQ_SCANNER = 5;
	private static final int REQ_MODEM = 6;
	private static final int DOC_DISCO = 7;
	
	
	protected int tempoInicializacao;
	protected int prioridade;
	protected int tempoProcessador;
	protected int blocosMemoria;
	protected int numImpressora;
	protected int reqScanner;
	protected int reqModem;
	protected int codDisco;

	/**
	 * Recebe uma string contendo as informacoes do processo:
	 * 
	 * @param req
	 */
	public RequisicaoProcesso(String req) {
		req = req.replace(" ", "");
		String[] aux = req.split(",");
		tempoInicializacao = Integer.valueOf(aux[TEMPO_INICIALIZACAO]);
		prioridade = Integer.valueOf(aux[PRIORIDADE]);
		tempoProcessador = Integer.valueOf(aux[TEMPO_PROCESSADOR]);
		blocosMemoria = Integer.valueOf(aux[BLOCOS_MEMORIA]);
		numImpressora = Integer.valueOf(aux[NUM_IMPRESSORA]);
		reqScanner = Integer.valueOf(aux[REQ_SCANNER]);
		reqModem = Integer.valueOf(aux[REQ_MODEM]);
		codDisco = Integer.valueOf(aux[DOC_DISCO]);
	}

	/**
	 * Compara as requisicoes por tempo de inicializacao
	 */
	@Override
	public int compareTo(RequisicaoProcesso aux) {
		return this.tempoInicializacao - aux.tempoInicializacao;
	}

	@Override
	public String toString() {
		return tempoInicializacao + ", " + prioridade + ", " + tempoProcessador
				+ ", " + blocosMemoria + ", " + numImpressora + ", "
				+ reqScanner + ", " + reqModem + ", " + codDisco;
	}

}
