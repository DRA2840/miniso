package br.unb.nso.pseudoSO.processo;

import java.util.Collections;

import br.unb.nso.pseudoSO.kernel.Kernel;

public class Prioridade extends Escalonador{

	
	public Prioridade(){
		super();
	}
	
	/**
	 * Escolhe um {@link Processo} (levando em conta a prioridade para processos de
	 * usuario e o tempo de entrada para processos de {@link Kernel}) para ser
	 * executado
	 * 
	 * @return {@link Processo} escolhido para ser executado
	 */
	public Processo escalonar() {
		System.out.println("\ndispatcher =>");
		Collections.shuffle(prontos);
		Collections.sort(prontos);
		return prontos.remove();
	}
}
