package br.unb.nso.pseudoSO.processo;

import br.unb.nso.pseudoSO.kernel.Kernel;

public class RoundRobin extends Escalonador{

	
	public RoundRobin(){
		super();
	}
	
	/**
	 * Escolhe um {@link Processo} (levando em conta a prioridade para processos de
	 * usuario e o tempo de entrada para processos de {@link Kernel}) para ser
	 * executado
	 * 
	 * @return {@link Processo} escolhido para ser executado
	 */
	public Processo escalonar() {
		System.out.println("\ndispatcher =>");
		Processo p = prontos.remove();
		prontos.add(p);
		return p;
	}
}
