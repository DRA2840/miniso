package br.unb.nso.pseudoSO.processo;

import java.util.LinkedList;
import java.util.List;

/**
 * Classe que desempenha as funcoes de escalonador
 * 
 * @author Diego Azevedo
 * 
 */
public abstract class Escalonador {

	protected final LinkedList<Processo> prontos;
	protected final LinkedList<Processo> bloqueados;

	/**
	 * Construtor padrao, inicializa tudo
	 */
	public Escalonador() {
		this.prontos = new LinkedList<Processo>();
		this.bloqueados = new LinkedList<Processo>();
	}

	/**
	 * Adiciona um {@link Processo}
	 * 
	 * @param p
	 *            {@link Processo} a ser adicionado
	 */
	public void adicionarProcesso(Processo p) {
		this.prontos.add(p);
	}

	/**
	 * Bloqueia um {@link Processo}
	 * 
	 * @param p
	 *            {@link Processo} a ser bloqueado
	 * @throws ProcessoBloqueadoException
	 *             sempre, para interromper o fluxo de execucao e obrigar o S.O.
	 *             a escalonar outro processo
	 */
	public void bloquearProcesso(Processo p) throws ProcessoBloqueadoException {
		System.out.println("\tProcesso Bloqueado!");
		this.prontos.remove(p);
		this.bloqueados.add(p);
		throw new ProcessoBloqueadoException();
	}

	/**
	 * Desbloqueia uma lista de processos que estava aguardando por determinado
	 * recurso. Nada impede que estes processos sejam bloqueados novamente
	 * posteriormente
	 * 
	 * @param p
	 *            {@link List} de {@link Processo}s a serem desbloqueados
	 */
	public void desbloquearProcessos(List<Processo> p) {
		this.bloqueados.removeAll(p);
		this.prontos.addAll(p);
	}

	/**
	 * Encerra um determinado {@link Processo}
	 * 
	 * @param p
	 *            {@link Processo} a ser encerrado
	 */
	public void encerrarProcesso(Processo p) {
		this.prontos.remove(p);
	}

	/**
	 * Envelhece todos os {@link Processo}s no estado de pronto
	 */
	public void envelhecerProcessos() {
		for (Processo p : prontos) {
			p.envelhecer();
		}
	}

	public abstract Processo escalonar();

	/**
	 * Retorna o numero de {@link Processo} na lista de prontos
	 * 
	 * @return numero de {@link Processo} na lista de prontos
	 */
	public int getNumProcessosProntos() {
		return this.prontos.size();
	}

	/**
	 * Retorna o numero total de {@link Processo}s
	 * 
	 * @return numero total de {@link Processo}s
	 */
	public int getNumTotalProcessos() {
		return this.prontos.size() + this.bloqueados.size();
	}
}
