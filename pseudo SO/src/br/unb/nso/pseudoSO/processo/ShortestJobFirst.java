package br.unb.nso.pseudoSO.processo;

import java.util.Collections;
import java.util.Comparator;

public class ShortestJobFirst extends Escalonador{

	@Override
	public Processo escalonar() {
		System.out.println("\ndispatcher =>");
		Collections.shuffle(prontos);
		Collections.sort(prontos, new Comparator<Processo>() {

			@Override
			public int compare(Processo o1, Processo o2) {
				o1.setPrioridade(0);
				o2.setPrioridade(0);
				return o1.getTempo() - o2.getTempo();
			}
		});
		
		
		return prontos.remove();
	}

}
