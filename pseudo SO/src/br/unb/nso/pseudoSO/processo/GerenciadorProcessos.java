package br.unb.nso.pseudoSO.processo;

import java.util.ArrayList;
import java.util.List;

import br.unb.nso.pseudoSO.kernel.Kernel;
import br.unb.nso.pseudoSO.memoria.MemoriaInsuficienteException;

/**
 * Classe responsavel por gerenciar as requisicoes de Processos
 * 
 * @author Diego Azevedo
 * 
 */
public class GerenciadorProcessos {

	private static final int NUMERO_MAXIMO_DE_PROCESSOS = 1000;
	private RequisicaoProcesso esperandoMemoria = null;

	private List<RequisicaoProcesso> requisicoes;

	/**
	 * Recebe as {@link RequisicaoProcesso}s
	 * 
	 * @param requisicoes
	 *            Requisicoes de novos processos
	 */
	public void receberRequisicoesDeProcessos(
			List<RequisicaoProcesso> requisicoes) {
		if (this.requisicoes != null) {
			this.requisicoes.addAll(requisicoes);
		}
		this.requisicoes = requisicoes;
	}

	/**
	 * Verifica se ha {@link RequisicaoProcesso} prontas para virarem {@link Processo}s e, se possivel,
	 * os cria junto do Kernel
	 * 
	 * @param kernel
	 *            Kernel do S.O.
	 * @param tempo
	 *            unidades de tempo passadas da inicializacao do S.O
	 * @return Lista de processos criados
	 */
	public synchronized List<Processo> novosProcessos(Kernel kernel, int tempo,
			int numeroTotalDeProcesos) {
		List<Processo> processos = new ArrayList<Processo>();
		List<RequisicaoProcesso> requisitados = new ArrayList<RequisicaoProcesso>();
		List<RequisicaoProcesso> deletar = new ArrayList<RequisicaoProcesso>();
		for (RequisicaoProcesso req : requisicoes) {
			if (req.tempoInicializacao <= tempo
					&& numeroTotalDeProcesos <= NUMERO_MAXIMO_DE_PROCESSOS) {
				try {
					if (kernel.existemBlocosContiguos(req.blocosMemoria,
							req.prioridade) && (esperandoMemoria == null || req.equals(this.esperandoMemoria) || req.prioridade == 0) ) {
						int offset = kernel.alocarMemoria(req.blocosMemoria,
								req.prioridade);
						Processo p = new Processo(req, offset);
						processos.add(p);
						requisitados.add(req);
						numeroTotalDeProcesos++;

						if(req.equals(this.esperandoMemoria)){
							this.esperandoMemoria = null;
						}
					} else {
						if( this.esperandoMemoria == null ){
							System.out
							.println("\nErro ao criar processo:\n"
									+ "   * A requisicao (" + req.toString() + ") precisa de mais memoria"
									+ " que disponivel no momento\n"
									+ "   * Aguardando encerramento de outros processos");
							this.esperandoMemoria = req;
						}
					}
				} catch (MemoriaInsuficienteException e) {
					System.out
					.println("\nErro ao criar processo:\n"
							+ "   * A requisicao (" + req.toString() + ") precisa de mais memoria"
							+ " que toda a memoria disponivel na maquina\n"
							+ "   * Removendo requisicao");
					deletar.add(req);
				}

			} else {
				break;
			}
		}
		requisicoes.removeAll(requisitados);
		requisicoes.removeAll(deletar);

		return processos;
	}

	/**
	 * Numero de requisicoes esperando para serem criadas
	 * 
	 * @return Numero de requisicoes esperando para serem criadas
	 */
	public int requisicoesEmEspera() {
		return this.requisicoes.size();
	}
}
