package br.unb.nso.pseudoSO.processo;

import br.unb.nso.pseudoSO.kernel.Kernel;

/**
 * Esta classe eh responsavel por representar um processo
 * 
 * @author Diego Azevedo
 * 
 */
public class Processo implements Comparable<Processo> {

	private static final int IDADE_MAXIMA = 10;
	private static final long TEMPO_REAL_POR_INSTRUCAO = 10;
	private static final int QUANTUM = 2;
	private static int numProcessos = 0;

	private final int pid;
	private int prioridade;
	private final int offset;
	private final int tamanho;
	private final int tempo;
	private final int impressora;
	private final int scanner;
	private final int modem;
	private final int drives;
	private int idade;
	private int tempoNaCpu;

	/**
	 * Inicializa um processo com base em sua requisicao e na primeira posicao
	 * de memoria alocada para ele
	 * 
	 * @param req
	 *            Requisicao de Processo original
	 * @param offset
	 *            posicao inicial na memoria alocada para este processo
	 */
	public Processo(RequisicaoProcesso req, int offset) {
		this.pid = ++numProcessos;

		this.prioridade = req.prioridade;
		this.offset = offset;
		this.tamanho = req.blocosMemoria;
		this.tempo = req.tempoProcessador;
		this.impressora = req.numImpressora;
		this.scanner = req.reqScanner;
		this.modem = req.reqModem;
		this.drives = req.codDisco;
		this.idade = 0;
		this.tempoNaCpu = 0;
		
		System.out.println("\nAdicionando Processo:\n" + this.toString());
	}

	/**
	 * Envelhece o processo
	 */
	public void envelhecer() {

		this.idade++;

		if (prioridade > 1 && idade >= IDADE_MAXIMA) {

			this.prioridade--;
			this.idade = 0;
			System.out
					.println("\nEnvelhecendo processo: \n\tP" + this.pid + "\n\tnew priority: " + this.prioridade);

		}
	}

	/**
	 * Executa o processo
	 * 
	 * @param kernel
	 *            Kernel responsavel por gerenciar o processo
	 * @return tempo de processamento
	 * @throws ProcessoBloqueadoException
	 *             caso o S.O. o bloqueie
	 */
	public int processar(Kernel kernel) throws ProcessoBloqueadoException {

		int tempo = QUANTUM;
		if (this.prioridade == 0) {
			tempo = this.tempo;
		}

		this.idade = 0;
		if (this.tempoNaCpu == 0 && prioridade != 0) {
			System.out.println("\tP" + this.pid
					+ "  Requisitando dispositivos:");
			kernel.requisitarDispositivos(this);

		}

		for (int i = 0; i < tempo && this.tempoNaCpu != this.tempo; i++) {
			esperar(TEMPO_REAL_POR_INSTRUCAO);
			System.out.println("\tP" + this.pid + "  instruction "
					+ ++this.tempoNaCpu);
		}

		if (this.tempoNaCpu == this.tempo) {
			if(prioridade != 0){
				System.out.println("\tP" + this.pid + "  Liberando dispositivos");
				kernel.liberarDispositivos(this);
			}
			kernel.encerrarProcesso(this);
			kernel.liberarMemoria(tamanho, offset);
		}

		return tempo;
	}

	/**
	 * Aguarda o tempo determindo
	 * 
	 * @param milis
	 *            tempo em milis
	 */
	private void esperar(long milis) {
		Long inicial = System.currentTimeMillis();
		Long now;
		do {
			now = System.currentTimeMillis();
		} while (inicial + milis > now);
	}
	
	/**
	 * Getter da variavel impressora
	 * 
	 * @return numero da impressora requisitada (0 caso nenhuma seja requisitada)
	 */
	public int getImpressora() {
		return impressora;
	}

	/**
	 * Getter da variavel scanner
	 * 
	 * @return numero do scanner requisitado (0 caso nenhum seja requisitado)
	 */
	public int getScanner() {
		return scanner;
	}

	public int getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(int prioridade) {
		this.prioridade = prioridade;
	}

	public int getTempo() {
		return tempo;
	}

	/**
	 * Getter da variavel modem
	 * 
	 * @return numero do modem requisitado (0 caso nenhum seja requisitado)
	 */
	public int getModem() {
		return modem;
	}

	/**
	 * Getter da variavel drives
	 * 
	 * @return numero do drive (disco) requisitada (0 caso nenhum seja requisitado)
	 */
	public int getDrives() {
		return drives;
	}

	/**
	 * Retorna as informacoes de um processo
	 */
	@Override
	public String toString() {
		return "\tPID: " + this.pid + "\n\toffset: " + this.offset
				+ "\n\tblocks: " + this.tamanho + "\n\tpriority: "
				+ this.prioridade + "\n\ttime: " + this.tempo
				+ "\n\tprinters: " + this.impressora + "\n\tscanners: "
				+ this.scanner + "\n\tmodem: " + this.modem + "\n\tdrives: "
				+ this.drives;

	}

	/**
	 * Compara os processos por <b>PRIORIDADE</b>, sendo que apenas os processos
	 * de Kernel sao comparados entre si por <b>ORDEM DE CHEGADA</b>
	 */
	@Override
	public int compareTo(Processo aux) {

		if (this.prioridade == 0 && aux.prioridade == 0) {
			return this.pid - aux.pid;
		}
		return this.prioridade - aux.prioridade;
	}
}