package br.unb.nso.pseudoSO.kernel;

import java.util.List;

import br.unb.nso.pseudoSO.dispositivo.Dispositivo;
import br.unb.nso.pseudoSO.dispositivo.GerenciadorDispositivos;
import br.unb.nso.pseudoSO.memoria.GerenciadorMemoria;
import br.unb.nso.pseudoSO.memoria.MemoriaInsuficienteException;
import br.unb.nso.pseudoSO.processo.Escalonador;
import br.unb.nso.pseudoSO.processo.Fifo;
import br.unb.nso.pseudoSO.processo.GerenciadorProcessos;
import br.unb.nso.pseudoSO.processo.Processo;
import br.unb.nso.pseudoSO.processo.ProcessoBloqueadoException;
import br.unb.nso.pseudoSO.processo.RequisicaoProcesso;

/**
 * Classe que exerce a funcao de Kernel, possibilitando a comunicacao entre os
 * diversos recursos do S.O.
 * 
 * @author Diego Azevedo
 * 
 */
public class Kernel {

	private int tempo;
	private final Escalonador escalonador;
	private final GerenciadorMemoria gerMem;
	private final GerenciadorProcessos gerPro;
	private final GerenciadorDispositivos gerDis;

	/**
	 * Construtor padrao, inicializa os recursos
	 */
	public Kernel() {
		escalonador = new Fifo();
		gerMem = new GerenciadorMemoria();
		gerPro = new GerenciadorProcessos();
		gerDis = new GerenciadorDispositivos();
		tempo = 0;
	}

	/**
	 * Metodo mais importante do Kernel, carrega os novos processos na CPU,
	 * escalona um processo dentre todos e o processa. Depois, envelhece os
	 * processos
	 */
	public void carregarProcessoNaCpu() {
		
		carregarProcessosValidos();

		try {
			Processo p = this.escalonador.escalonar();
			
			// Adiciona o tempo de execucao do processo
			tempo += p.processar(this);
			
			this.escalonador.envelhecerProcessos();
		} catch (IndexOutOfBoundsException e) {
			// caso nao tenha nenhum processo pronto, mas hava uma
			// requisicao, incrementa 1 unidade de tempo
			tempo++; 
		} catch (ProcessoBloqueadoException e) {
			// caso o processo seja bloqueado, nao faz nada, pois o loop vai se repetir
			// e um novo processo será imediatamente escalonado
		}

	}

	/**
	 * Obtem a lista de novos processos do {@link GerenciadorProcessos} e passa para
	 * o {@link Escalonador}
	 */
	public void carregarProcessosValidos() {

		List<Processo> processos = this.gerPro.novosProcessos(this, this.tempo,
				this.escalonador.getNumTotalProcessos());

		for (Processo p : processos) {
			this.escalonador.adicionarProcesso(p);
		}
	}

	/**
	 * Verifica junto ao {@link Escalonador} e ao {@link GerenciadorProcessos} se existe
	 * algum processo que pode ser executado no futuro.
	 * 
	 * @return true, caso exista algum processo a ser executado, false, caso
	 *         contrario
	 */
	public boolean existemProcessosASeremExecutados() {
		return this.escalonador.getNumProcessosProntos() > 0
				|| this.gerPro.requisicoesEmEspera() > 0;
	}

	/* * * * * * * * * * * * * * * * * * * * * * * * * *
	 * A partir daqui, requisicoes para o Escalonador
	 */

	/**
	 * Manda o {@link Escalonador} encerrar um processo
	 * 
	 * @param p
	 *            Processo que deve ser encerrado
	 */
	public void encerrarProcesso(Processo p) {
		this.escalonador.encerrarProcesso(p);
	}

	/**
	 * Manda o {@link Escalonador} desbloquear uma lista de processos.
	 * 
	 * @param processos
	 *            Lista de processos que devem ser desbloqueados
	 */
	public void desbloquearProcessos(List<Processo> processos) {
		this.escalonador.desbloquearProcessos(processos);
	}

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * A partir daqui, requisicoes para o Gerenciador de Memoria
	 */

	/**
	 * Verifica, junto ao {@link GerenciadorMemoria}, se existem Blocos contiguos
	 * 
	 * @param numBlocos
	 *            Numero de blocos contiguos que se deseja
	 * @param prioridade
	 *            Prioridade do processo que deseja esses blocos
	 * @return true se houverem blocos disponiveis, false caso contrario
	 */
	public boolean existemBlocosContiguos(int numBlocos, int prioridade)
			throws MemoriaInsuficienteException {
		return this.gerMem.existemBlocosContiguos(numBlocos, prioridade);
	}

	/**
	 * Aloca, junto ao {@link GerenciadorMemoria}, o numero de blocos necessarios
	 * para um processo
	 * 
	 * @param tamanho
	 *            Numero de blocos alocados para o processo
	 * @param prioridade
	 *            Prioridade do processo
	 * @return <b>OFFSET na memoria</b>
	 */
	public int alocarMemoria(int tamanho, int prioridade) {
		return this.gerMem.alocarMemoria(tamanho, prioridade);
	}

	/**
	 * Libera, junto ao {@link GerenciadorMemoria}, um determinado numero de blocos
	 * 
	 * @param tamanho
	 *            Numero de Blocos liberados
	 * @param offset
	 *            local de inicio da liberacao
	 */
	public void liberarMemoria(int tamanho, int offset) {
		this.gerMem.liberarMemoria(tamanho, offset);
	}

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * A partir daqui, requisicoes para o Gerenciador de Dispositivos
	 */

	/**
	 * Requisita, junto ao {@link GerenciadorDispositivos} todos os {@link Dispositivo}s necessarios para a execucao do {@link Processo} p
	 * 
	 * @param p
	 *            {@link Processo} requisitando a Impressora 1
	 * @throws ProcessoBloqueadoException
	 *             Caso <b>QUALQUER</b> dispositivo requisitado nao esteja disponivel
	 */
	public void requisitarDispositivos(Processo p) throws ProcessoBloqueadoException {
		try {
			this.gerDis.requisitarDipositivos(p);
		} catch (ProcessoBloqueadoException e) {
			this.escalonador.bloquearProcesso(p);
			
		}
	}

	/**
	 * Libera, junto ao {@link GerenciadorDispositivos} todos os {@link Dispositivo}s alocados para um {@link Processo} p
	 * 
	 * @param processo {@link Processo} liberando dispositivos
	 */
	public void liberarDispositivos(Processo processo) {
		this.gerDis.liberarDispositivos(this, processo);
		
	}

	/* * * * * * * * * * * * * * * * * * * * * * * *
	 * Requisicao para o Gerenciador de Processos 
	 */

	/**
	 * Manda o Gerenciador de Processos carregar uma lista de requisicoes
	 * 
	 * @param requisicoes
	 *            Lista de Requisicoes de Processos
	 */
	public void carregarRequisicoesProcessos(
			List<RequisicaoProcesso> requisicoes) {
		this.gerPro.receberRequisicoesDeProcessos(requisicoes);
	}


}
