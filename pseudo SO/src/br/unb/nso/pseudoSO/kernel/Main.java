package br.unb.nso.pseudoSO.kernel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.unb.nso.pseudoSO.processo.RequisicaoProcesso;

/**
 * Classe main, responsavel por ler o arquivo com a lista de processos e manter
 * o Kernel rodando enquanto houverem processos nao executados
 * 
 * @author Diego Azevedo
 * 
 */
public class Main{
	
	/**
	 * Metodo main, mantem o Kernel rodando enquanto houverem processos
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		List<RequisicaoProcesso> processos = obtemProcessosEmOrdem();

		Kernel kernel = new Kernel();
		kernel.carregarRequisicoesProcessos(processos);

		while (kernel.existemProcessosASeremExecutados()) {
			kernel.carregarProcessoNaCpu();
		}
		System.out.println("Acabaram os processos!");

	}


	/**
	 * Metodo que interage com o usuario, le as linhas do arquivo passado e cria
	 * as respectivas requisicoes. Depois, ordena essas requisicoes <b>por TEMPO
	 * </b>
	 * 
	 * @return Lista de Requisicoes ordenadas
	 */
	private static List<RequisicaoProcesso> obtemProcessosEmOrdem() {

		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		List<RequisicaoProcesso> processos = new ArrayList<RequisicaoProcesso>();
		String path;
		FileReader arquivo;
		BufferedReader leitor;

		try {
			// Pede o arquivo para o usuario
			System.out
					.println("Caminho para a lista de processos a serem executados: ");
			path = in.readLine();
			arquivo = new FileReader(new File(path));
			leitor = new BufferedReader(arquivo);

			// Le todas as linhas do arquivo, criando suas respectivas
			// requisicoes
			while (leitor.ready()) {
				processos.add(new RequisicaoProcesso(leitor.readLine()));
			}

			leitor.close();
			arquivo.close();

		} catch (IOException e) {
			// Houve problema na leitura do arquivo
			System.out.println("Houve um problema na leitura do seu arquivo.");
			System.out.println("Verifique se o caminho esta correto.");
		} catch (IndexOutOfBoundsException e) {
			// Houve problema na criacao do processo
			System.out.println("Houve um problema na leitura do seu arquivo.");
			System.out.println("Verifique se ele esta devidamente formatado");
		} finally {
			// Deixo para o GC() limpar tudo
			arquivo = null;
			leitor = null;
		}

		// Ordena as requisicoes por TEMPO
		Collections.sort(processos);

		return processos;
	}
}
