package br.unb.nso.pseudoSO.memoria;

/**
 * Classe que realiza as operacoes de um gerenciador de memoria
 * 
 * @author Diego Azevedo
 * 
 */
public class GerenciadorMemoria {

	private static final int MEM_KERNEL = 64;
	private static final int MEM_USUARIO = 960;

	private final boolean[] memKernel = new boolean[MEM_KERNEL];
	private final boolean[] memUsuario = new boolean[MEM_USUARIO];

	/**
	 * contrutor padrao, inicializa o mapa de bits
	 */
	public GerenciadorMemoria() {
		for (int i = 0; i < MEM_KERNEL; i++) {
			memKernel[i] = true;
		}
		for (int i = 0; i < MEM_USUARIO; i++) {
			memUsuario[i] = true;
		}
	}

	/**
	 * Verifica se existe um determinado numero de blocos contiguos considerando
	 * a prioridade do processo
	 * 
	 * @param numBlocos
	 *            Numero de blocos contiguos
	 * @param prioridade
	 *            Prioridade do processo requisitando a memoria
	 * @return true se houverem blocos contiguos, false caso contrario
	 */
	public boolean existemBlocosContiguos(int numBlocos, int prioridade)
			throws MemoriaInsuficienteException {
		if (numBlocos > MEM_USUARIO || prioridade == 0
				&& numBlocos > MEM_KERNEL) {
			throw new MemoriaInsuficienteException();
		}
		boolean teste = false;
		int aux = 0;
		if (prioridade == 0) {
			for (int i = 0; i < MEM_KERNEL; i++) {
				if (memKernel[i]) {
					aux++;
				} else {
					aux = 0;
				}
				if (aux == numBlocos) {
					teste = true;
					break;
				}
			}
		} else {
			for (int i = 0; i < MEM_USUARIO; i++) {
				if (memUsuario[i]) {
					aux++;
				} else {
					aux = 0;
				}
				if (aux == numBlocos) {
					teste = true;
					break;
				}
			}
		}

		return teste;
	}

	/**
	 * Aloca na memoria, de forma contigua, o numero de blocos passados
	 * 
	 * @param tamanho
	 *            tamanho da memoria a ser alocada de forma contigua
	 * @param prioridade
	 *            Prioridade do processo requisitando memoria
	 * @return posicao do primeiro bloco alocado
	 */
	public int alocarMemoria(int tamanho, int prioridade) {
		int aux = 0;
		int offset = 0;
		if (prioridade == 0) {
			for (int i = 0; i < MEM_KERNEL; i++) {
				if (memKernel[i]) {
					aux++;
				} else {
					aux = 0;
				}
				if (aux == tamanho) {
					offset = i - aux + 1;

					for (int j = offset; j < aux + offset; j++) {
						memKernel[j] = false;
					}
					break;
				}
			}
		} else {
			for (int i = 0; i < MEM_USUARIO; i++) {
				if (memUsuario[i]) {
					aux++;
				} else {
					aux = 0;
				}
				if (aux == tamanho) {
					offset = i - aux + 1;

					for (int j = offset; j < aux + offset; j++) {
						memUsuario[j] = false;
					}
					offset += 64;
					break;
				}
			}
		}
		return offset;
	}

	/**
	 * Libera a memoria previamente alocada
	 * 
	 * @param tamanho
	 *            numero de blocos a serem liberados
	 * @param offset
	 *            posicao inicial dos blocos
	 */
	public void liberarMemoria(int tamanho, int offset) {
		if(offset < 64){
			for(int i = offset; i<tamanho + offset;i++){
				memKernel[i] = true;
			}
		}else{
			offset -= 64;
			for(int i = offset; i<tamanho + offset;i++){
				memUsuario[i] = true;
			}
		}
	}

}
