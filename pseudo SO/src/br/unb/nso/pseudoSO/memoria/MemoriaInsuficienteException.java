package br.unb.nso.pseudoSO.memoria;

/**
 * Exception lancada quando nao o sistema nao comporta memoria suficiente para
 * um processo com determinada prioridade
 * 
 * @author Diego Azevedo
 * 
 */
@SuppressWarnings("serial")
public class MemoriaInsuficienteException extends Exception {

}
