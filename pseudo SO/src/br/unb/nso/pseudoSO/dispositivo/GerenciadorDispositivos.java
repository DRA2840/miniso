package br.unb.nso.pseudoSO.dispositivo;

import java.util.LinkedList;
import java.util.List;

import br.unb.nso.pseudoSO.kernel.Kernel;
import br.unb.nso.pseudoSO.processo.Processo;
import br.unb.nso.pseudoSO.processo.ProcessoBloqueadoException;

/**
 * Classe responsavel por gerenciar os {@link Dispositivo}s
 * 
 * @author Diego Azevedo
 * 
 */
public class GerenciadorDispositivos {

	private Dispositivo impressora1;
	private List<Processo> bloqImp1;
	private Dispositivo impressora2;
	private List<Processo> bloqImp2;
	private Dispositivo scanner;
	private List<Processo> bloqScan;
	private Dispositivo modem;
	private List<Processo> bloqMod;
	private Dispositivo disco1;
	private List<Processo> bloqDis1;
	private Dispositivo disco2;
	private List<Processo> bloqDis2;
	private DispositivoMemento historico;

	/**
	 * Construtor padrao
	 */
	public GerenciadorDispositivos() {
		this.impressora1 = new Dispositivo();
		this.bloqImp1 = new LinkedList<Processo>();
		this.impressora2 = new Dispositivo();
		this.bloqImp2 = new LinkedList<Processo>();
		this.scanner = new Dispositivo();
		this.bloqScan = new LinkedList<Processo>();
		this.modem = new Dispositivo();
		this.bloqMod = new LinkedList<Processo>();
		this.disco1 = new Dispositivo();
		this.bloqDis1 = new LinkedList<Processo>();
		this.disco2 = new Dispositivo();
		this.bloqDis2 = new LinkedList<Processo>();
		this.historico = new DispositivoMemento();
	}

	/**
	 * Salva este estado como ultimo estado valido
	 */
	public void commit() {		
		this.historico.setImpressora1(this.impressora1.clone());
		this.historico.setImpressora2(this.impressora2.clone());
		this.historico.setModem(this.modem.clone());
		this.historico.setScanner(this.scanner.clone());
		this.historico.setDisco1(this.disco1.clone());
		this.historico.setDisco2(this.disco2.clone());
	
	}

	/**
	 * Retorna para o ultimo estado valido
	 */
	public void rollback() {
		
			this.impressora1 = this.historico.getImpressora1();
			this.impressora2 = this.historico.getImpressora2();
			this.scanner = this.historico.getScanner();
			this.modem =  this.historico.getModem();
			this.disco1 =  this.historico.getDisco1();
			this.disco2 =  this.historico.getDisco2();
	}

	/**
	 * Verifica todos os {@link Dispositivo}s requisitados por um {@link Processo} p
	 * E os aloca para o processo caso <b>TODOS</b> estejam disponiveis
	 * 
	 * @param p {@link Processo} requisitando dispositivos
	 * @throws ProcessoBloqueadoException Caso um dos dispositivos nao possa ser alocado
	 */
	public synchronized void requisitarDipositivos(Processo p) throws ProcessoBloqueadoException {

		this.commit();
		try {
			// requisita impressoras
			if (p.getImpressora() == 1) {
				System.out.println("\t   * Impressora1");
				if( !this.impressora1.down()){
					this.bloqImp1.add(p);
					throw new ProcessoBloqueadoException();
				}
			} else if (p.getImpressora() == 2) {
				System.out.println("\t   * Impressora2");
				if( !this.impressora2.down()){
					this.bloqImp2.add(p);
					throw new ProcessoBloqueadoException();
				}
			}
			// requisita modem
			if (p.getModem() == 1) {
				System.out.println("\t   * Modem");
				if( !this.modem.down()){
					this.bloqMod.add(p);
					throw new ProcessoBloqueadoException();
				}
			}
			// requisita scanner
			if (p.getScanner() == 1) {
				System.out.println("\t   * Scanner");
				if( !this.scanner.down()){
					this.bloqScan.add(p);
					throw new ProcessoBloqueadoException();
				}
			}
			// requisita discos
			if (p.getDrives() == 1) {
				System.out.println("\t   * Disco1");
				if( !this.disco1.down()){
					this.bloqDis1.add(p);
					throw new ProcessoBloqueadoException();
				}
			} else if (p.getDrives() == 2) {
				System.out.println("\t   * Disco2");
				if( !this.disco2.down()){
					this.bloqDis2.add(p);
					throw new ProcessoBloqueadoException();
				}
			}
		} catch (ProcessoBloqueadoException e) {
			this.rollback();
			throw e;
		}
	}

	/**
	 * Libera todos os {@link Dispositivo}s alocados para um {@link Processo} p e
	 * desbloqueia todos os {@link Processo}s que estavam esperando os {@link Dispositivo}s serem liberados
	 * 
	 * @param kernel {@link Kernel} responsavel por desbloquear os {@link Processo}s esperando dispositivos
	 * @param p {@link Processo} que vai liberar seus dispositivos
	 */
	public synchronized void liberarDispositivos(Kernel kernel, Processo p) {
			// libera impressoras
			if (p.getImpressora() == 1) {
				System.out.println("\t   * Impressora1");
				this.impressora1.up();
				kernel.desbloquearProcessos(bloqImp1);
				bloqImp1.clear();
			} else if (p.getImpressora() == 2) {
				System.out.println("\t   * Impressora2");
				this.impressora2.up();
				kernel.desbloquearProcessos(bloqImp2);
				bloqImp2.clear();
			}
			// libera modem
			if (p.getModem() == 1) {
				System.out.println("\t   * Modem");
				this.modem.up();
				kernel.desbloquearProcessos(bloqMod);
				bloqMod.clear();
			}
			// libera scanner
			if (p.getScanner() == 1) {
				System.out.println("\t   * Scanner");
				this.scanner.up();
				kernel.desbloquearProcessos(bloqScan);
				bloqScan.clear();
			}
			// libera discos
			if (p.getDrives() == 1) {
				System.out.println("\t   * Disco1");
				this.disco1.up();
				kernel.desbloquearProcessos(bloqDis1);
				bloqDis1.clear();
			} else if (p.getDrives() == 2) {
				System.out.println("\t   * Disco2");
				this.disco2.up();
				kernel.desbloquearProcessos(bloqDis2);
				bloqDis2.clear();
			}	
	}

}
