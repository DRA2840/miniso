package br.unb.nso.pseudoSO.dispositivo;

/**
 * Classe que representa um dispositivo de I/O
 * 
 * @author diego
 * 
 */
public class Dispositivo implements Cloneable{

	private Boolean mutex;

	/**
	 * Construtor padrao, inicia o dispositivo como disponivel
	 */
	public Dispositivo() {
		mutex = true;
	}

	/**
	 * Tenta alocar o dispositivo
	 * 
	 * @return true, se for possivel alocar, false caso contrario
	 */
	public synchronized boolean down() {
		if (this.mutex) {
			this.mutex = false;
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Libera o dispositivo
	 */
	public synchronized void up() {
		this.mutex = true;
	}

	/**
	 * Contrutor privado usado no metodo clone()
	 * 
	 * @param mutex Mutex atual
	 */
	private Dispositivo(boolean mutex) {
		this.mutex = mutex;
	}

	/**
	 * Gera um novo dispositivo com a exata mesma configuracao do atual
	 */
	@Override
	public Dispositivo clone() {
		return new Dispositivo(this.mutex);
	}

}
