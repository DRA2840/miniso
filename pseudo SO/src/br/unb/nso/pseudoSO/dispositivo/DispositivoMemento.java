package br.unb.nso.pseudoSO.dispositivo;

/**
 * Classe que salva o estado do {@link GerenciadorDispositivos}. Usada no Pattern Memento.
 * 
 * @author Diego Azevedo
 *
 */
public class DispositivoMemento {
	private Dispositivo impressora1;
	private Dispositivo impressora2;
	private Dispositivo scanner;
	private Dispositivo modem;
	private Dispositivo disco1;
	private Dispositivo disco2;

	/**
	 * Getter de impressora1
	 * 
	 * @return {@link Dispositivo} impressora1
	 */
	public Dispositivo getImpressora1() {
		return impressora1;
	}
	
	/**
	 * Setter de impressora1
	 * 
	 * @param impressora1 {@link Dispositivo} impressora1
	 */
	public void setImpressora1(Dispositivo impressora1) {
		this.impressora1 = impressora1;
	}

	/**
	 * Getter de impressora2
	 * 
	 * @return {@link Dispositivo} impressora2
	 */
	public Dispositivo getImpressora2() {
		return impressora2;
	}

	/**
	 * Setter de impressora2
	 * 
	 * @param impressora2 {@link Dispositivo} impressora2
	 */
	public void setImpressora2(Dispositivo impressora2) {
		this.impressora2 = impressora2;
	}

	/**
	 * Getter de scanner
	 * 
	 * @return {@link Dispositivo} scanner
	 */
	public Dispositivo getScanner() {
		return scanner;
	}

	/**
	 * Setter de scanner
	 * 
	 * @param scanner {@link Dispositivo} scanner
	 */
	public void setScanner(Dispositivo scanner) {
		this.scanner = scanner;
	}

	/**
	 * Getter de modem
	 * 
	 * @return {@link Dispositivo} modem
	 */
	public Dispositivo getModem() {
		return modem;
	}

	/**
	 * Setter de modem
	 * 
	 * @param modem {@link Dispositivo} modem
	 */
	public void setModem(Dispositivo modem) {
		this.modem = modem;
	}

	/**
	 * Getter de disco1
	 * 
	 * @return {@link Dispositivo} disco1
	 */
	public Dispositivo getDisco1() {
		return disco1;
	}

	/**
	 * Setter de disco1
	 * 
	 * @param disco1 {@link Dispositivo} disco1
	 */
	public void setDisco1(Dispositivo disco1) {
		this.disco1 = disco1;
	}

	/**
	 * Getter de disco2
	 * 
	 * @return {@link Dispositivo} disco2
	 */
	public Dispositivo getDisco2() {
		return disco2;
	}

	/**
	 * Setter de disco2
	 * 
	 * @param disco2 {@link Dispositivo} disco2
	 */
	public void setDisco2(Dispositivo disco2) {
		this.disco2 = disco2;
	}
}
